FROM python:3.11-alpine

WORKDIR /app

COPY requirements.txt requirements.txt

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 9999

CMD ["sh", "-c", "pytest -s -v -n=2 --alluredir=allure-result"]
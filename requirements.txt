allure-pytest==2.13.2
allure-python-commons==2.13.2
pytest==7.4.0
pytest-xdist==3.3.1
